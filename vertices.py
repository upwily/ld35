import turtle
from tkinter import filedialog

vertices = [(0,0)]

def clicked(x, y):
    global vertices, t
    t.goto(x, y)
    vertices.append((x,y))

t = turtle.Turtle()
s = turtle.Screen()

s.onclick(clicked)
turtle.mainloop()


f = filedialog.asksaveasfile(mode='w', initialdir="Assets/Vertices/", filetypes =(("Comma separated values", "*.csv"),("All Files","*.*")), defaultextension ="csv")
for v in vertices:
    f.write(str(v[0]/100)+";"+str(v[1]/100)+"\n")
f.close()
