﻿using UnityEngine;
using System.Collections;

public class Bee : MonoBehaviour {
    public float offset = 0;
	public float speed;
    public Vector2 end;
    private Vector2 path;
	private Vector2 start;
	private float last;

	// Use this for initialization
	void Start () {
		start = new Vector2 (transform.position.x, transform.position.y);
		transform.Rotate (new Vector3 (180, 0, 180));
        path = end - start;
		last = Mathf.Sin(Time.time);
		if (end.x < start.x) {
			transform.Rotate (new Vector3 (0, 180, 0)); 
		}
	}
	
	// Update is called once per frame
	void Update () {
		float currentTime = Time.time * speed * Mathf.PI + offset * Mathf.PI;
		transform.position = (Vector3)(start + path * (Mathf.Cos(currentTime)+1)/2);
        //Vector2 d = path * Mathf.Sin(Time.time);
        //Quaternion direction = new Quaternion(Mathf.Cos(Time.time / 2f), Mathf.Sin(Time.time / 2f), 0, 0);
        //transform.rotation = direction;
		if (last * Mathf.Sin (currentTime) <= 0) {
			transform.Rotate (0, 180, 0);
		}
		last = Mathf.Sin(currentTime);
    }
}
