﻿using UnityEngine;
using System.Collections;

public class rooootate : MonoBehaviour {

    public bool counteClockwise;
    private float angle;
	// Use this for initialization
	void Start () {
        angle = counteClockwise ? 0.02f : -0.01f;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.RotateAround(new Vector3(0, 0, 1), angle);
	}
}
