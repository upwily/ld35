﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TransPlatformer : MonoBehaviour {
	public Mesh initial;
	public Mesh flip;
    public bool DirectMorph;
    public float speed = 1;
	private Vector3[] newInit;
	private Vector3[] newFlip;
	private Mesh mesh;
    private PolygonCollider2D polycol;
    private Rigidbody2D rigidbody;

	Vector2[] Vertices3Dto2D(Vector3[] veclist) // teeb Vector3 jada Vector2 jadaks
	{

		int size = veclist.Length;
		Vector2[] temp = new Vector2[size];
		for(int i=0;i<size;i++)
		{
			temp [i] = veclist [i];
		}

		return temp;
	}

	float isCounter (Vector3 a, Vector3 b) {// V6tab 2 vektorit
		if (a == Vector3.zero || b == Vector3.zero) { // kui yks vektoritest on null, tagastab nulli (nagu oleks paraleelsed)
			return 0;
		}
        float angleA = 0;
        if (a.y == 0)
        {
            angleA = a.x > 0 ? Mathf.PI / 2 : 3 * Mathf.PI / 2;
        }
        else
		    angleA = Mathf.Atan (a.x / a.y); // nurgad algavad postitiivse y-telje osast, suurenevad p2rip2eva, l2heb lolliks, kui vektor on paralleelne x-teljega
        if (a.x < 0 && a.y > 0) {
			angleA = 2 * Mathf.PI + angleA;
		} else if (a.y < 0) {
			angleA = Mathf.PI + angleA;
		}
        float angleB = 0;
        if(b.y == 0)
        {
            angleB = b.x > 0 ? Mathf.PI / 2 : 3 * Mathf.PI / 2;
        }
        else
            angleB = Mathf.Atan (b.x / b.y);
		if (b.x < 0 && b.y > 0) {
			angleB = 2 * Mathf.PI + angleB;
		} else if (b.y < 0) {
			angleB = Mathf.PI + angleB;
		}
		return angleB - angleA; // Kui positiivne, siis B on nurk on suurem
	}

	Vector3 cross (Vector3 o, Vector3 r, Vector3 a, Vector3 b) { // leiab ristumispunkti kahe sirge vahel, kui paralleelsed, siis jagab 0-iga
		float x = ((o.x*r.y-o.y*r.x)*(a.x-b.x)-(o.x-r.x)*(a.x*b.y-a.y*b.x))/((o.x-r.x)*(a.y-b.y)-(o.y-r.y)*(a.x-b.x));
		float y = ((o.x*r.y-o.y*r.x)*(a.y-b.y)-(o.y-r.y)*(a.x*b.y-a.y*b.x))/((o.x-r.x)*(a.y-b.y)-(o.y-r.y)*(a.x-b.x));
		return new Vector3(x, y);
	}

	int[] MakeTriangles(int length){
		List<int> triangles = new List<int> ();
		for (int i = 1; i < length -1; i++) {
			triangles.Add (0);
			triangles.Add (i);
			triangles.Add (i + 1);
		}
		triangles.Add (0);
		triangles.Add (length-1);
		triangles.Add (1);
		return triangles.ToArray ();
	}

	void InitAnimation() {
		Vector3 transformer = initial.vertices[0] - flip.vertices[0]; // Vektor flipi keskpunktist initi keskpunkti
		int initSize = initial.vertices.Length; // muutuja 
		int flipSize = flip.vertices.Length;// muutuja
        Vector3[] flipVertices = flip.vertices;
		Vector3[] transformedFlip = new Vector3[flipSize]; // uus ajutine array
		for (int i = 0; i < flipSize; i++) { // k2ib k6ik flipi elemendid l2bi ja nihutab nii, et kujundite keskpunktid satuvad kokku
			transformedFlip [i] = flipVertices[i] + transformer;
		}
        flipVertices = transformedFlip; //paneb ajutise v22rtuse tagasi

        if (DirectMorph && (initSize == flipSize))
        {
            newInit = initial.vertices;
            newFlip = flip.vertices;
            mesh = new Mesh();
            mesh.vertices = initial.vertices;
            mesh.triangles = MakeTriangles(initial.vertices.Length);
            GetComponent<MeshFilter>().mesh = mesh;

        }
        else
        {
            int initI = 1, flipI = 1; // 0 elementi pole vaja animeerida, kuna see on juba paigas
            newInit = new Vector3[initSize + flipSize - 1]; // keskpunkt on yhine, teised pubktid liidab kokku uue kujundi jaoks
            newInit[0] = initial.vertices[0]; // m22rab 2ra 6ige keskkpunkti (sama mis originaalsel kujundil)
            newFlip = new Vector3[initSize + flipSize - 1]; // sama loogika flipi kujundiga
            newFlip[0] = flip.vertices[0];
            int matchingRadians = 0; // mitu korda kiire suunad olid samad
            for (int i = 1; i < initSize + flipSize - 1 - matchingRadians; i++)
            { // alustab yhest, k2ib m6lema punktid l2bi aga ei arvesta keskmist (0) punkti
                if (initI != initSize && flipI != flipSize && isCounter(initial.vertices[initI] - initial.vertices[0], flipVertices[flipI] - flipVertices[0]) == 0)
                {//Kui indeksid pole yle l2inud ja tippude suunad on paralleelsed
                    newInit[i] = initial.vertices[initI]; // m22rab uutele kujunditele samad tipud
                    newFlip[i] = flipVertices[flipI];
                    initI++;// liigub m6lema tipu indeksiga edasi, kuna m6lemad vaadatud
                    flipI++;
                    matchingRadians++;// olid paralleelsed, v2hendab loopi, kuna vaatas yhe k2iguga juba 2 tippu
                }
                else if (initI != initSize && (flipI == flipSize || isCounter(initial.vertices[initI] - initial.vertices[0], flipVertices[flipI] - flipVertices[0]) > 0))
                {//Kui initi indeks pole yle l2inud ja flipi indeks on / vaadatavatel tippudel on flipi tipp kraadides kaugemal
                    int tempFlipIB = flipI == 1 ? flipSize : flipI; // Kui on flipi esimene tipp, siis on v22rtus maksimaalne flipi tippude arv, kui ei ole, siis on v22rtus praegune flipi v22rtus
                    int tempFlipIA = flipI == flipSize ? 1 : flipI; // Kui on juba yle l2inud, siis on esimene tipp, muidu sama
                    newInit[i] = initial.vertices[initI];//v22rtustab initi tipu 2ra
                    newFlip[i] = cross(initial.vertices[0], initial.vertices[initI], flipVertices[tempFlipIB - 1], flipVertices[tempFlipIA]);//Loob vahepealse punkti flipi jaoks
                    initI++; // suurendab ainult init indeksit
                }
                else {//Kui uuele init mudelile lisatakse lisa tipp
                    int tempInitIB = initI <= 1 ? initSize : initI;// miks v2iksem kui?
                    int tempInitIA = initI >= initSize ? 1 : initI;
                    newFlip[i] = flipVertices[flipI];
                    newInit[i] = cross(flipVertices[0], flipVertices[flipI], initial.vertices[tempInitIB - 1], initial.vertices[tempInitIA]);
                    flipI++;// t66tab analoogselt eelmisegas
                }
            }
            Array.Resize<Vector3>(ref newInit, newInit.Length - matchingRadians);
            Array.Resize<Vector3>(ref newFlip, newFlip.Length - matchingRadians);
            mesh = new Mesh();
            mesh.vertices = newInit;
            mesh.triangles = MakeTriangles(newInit.Length);
            GetComponent<MeshFilter>().mesh = mesh;
        }
	}

    void Awake()
    {
        if(GetComponent<PolygonCollider2D>()==null)
            polycol = gameObject.AddComponent<PolygonCollider2D>() as PolygonCollider2D;
        if (GetComponent<Rigidbody2D>() == null)
            rigidbody = gameObject.AddComponent<Rigidbody2D>() as Rigidbody2D;
        else
            rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.isKinematic = true;
        rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        
    }

	void Start () {
		InitAnimation ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		Vector3[] current = new Vector3[newInit.Length];
		for (int i = 0; i < newInit.Length; i++) {
			Vector3 path = (newFlip [i] - newInit [i])*(Mathf.Cos(Time.time*speed*Mathf.PI)+1)/2;
			current [i] = newInit [i] + path;
		}
		mesh.vertices = current;
        GetComponent<MeshFilter>().sharedMesh.RecalculateBounds();

        var foos = new List<Vector3>(current);
        foos.RemoveAt(0);
        GetComponent<PolygonCollider2D>().points = Vertices3Dto2D (foos.ToArray());
	}
}
