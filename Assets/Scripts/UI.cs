﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;


public class UI: MonoBehaviour
{
    public bool isMenu = false;
    public int currentLevel = 1;
    public Sprite upsideDown;
    public Sprite sadTortoiseAndBee;
    public Sprite happyEnd;
    public bool paused;
    void Start ()
    {
        if (!isMenu) {
            GetComponent<Canvas>().enabled = false; // no gui during game
        }
        else // if is menu
        {
            UnityEngine.UI.Dropdown d = gameObject.GetComponentInChildren<UnityEngine.UI.Dropdown>();
            if (d != null)
            {
                d.value = PlayerPrefs.GetInt("level");
            }
        }
        Time.timeScale = 1;
    }

    public void pausegame()
    {
        GetComponent<Canvas>().enabled = true;
        Time.timeScale = 0;
        paused = true;
    }
    public void continuegame()
    {
        GetComponent<Canvas>().enabled = false;
        Time.timeScale = 1;
        paused = false;
    }

    public void realpausegame ()
    {
        gameObject.transform.GetChild(2).gameObject.SetActive(true);
        gameObject.transform.GetChild(3).gameObject.SetActive(false);
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        pausegame();
    }
    public void dead ()
    {
        pausegame();
        gameObject.GetComponentInChildren<Text>().text = "DEAD!";
		gameObject.transform.GetChild(2).gameObject.SetActive(false);
		gameObject.transform.GetChild(3).gameObject.SetActive(false);
    }

    public void fallen()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        gameObject.GetComponentInChildren<Image>().sprite = upsideDown;
        dead();
    }
    public void stung()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        gameObject.GetComponentInChildren<Image>().sprite = sadTortoiseAndBee;
        dead();
    }
    public void win()
    {
        pausegame();
        gameObject.GetComponentInChildren<Text>().text = "CONGRATULATIONS!";
        PlayerPrefs.SetInt("level", currentLevel); // currentLevel is 1-indexed, PlayerPrefs is 0-indexed
        gameObject.transform.GetChild(2).gameObject.SetActive(false);
        gameObject.transform.GetChild(3).gameObject.SetActive(true);
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        gameObject.GetComponentInChildren<Image>().sprite = happyEnd;
    }

    public void reload ()
    {
        continuegame();
        Application.LoadLevel(Application.loadedLevel);
    }

    public void tomain ()
    {
        continuegame();
        Application.LoadLevel(0);
    }

    public void play()
    {
        if (PlayerPrefs.GetInt("level") + 1 > 7)
        {
            tomain();
            return;
        }
        continuegame();
        Application.LoadLevel(PlayerPrefs.GetInt("level") + 1);
        Debug.Log(PlayerPrefs.GetInt("level") + 1);
    }

    public void updated()
    {
        UnityEngine.UI.Dropdown d = gameObject.GetComponentInChildren<UnityEngine.UI.Dropdown>();
        Debug.Log(d.value);
        PlayerPrefs.SetInt("level", d.value);
    }

    public void quit()
    {
        Application.Quit();
    }
    public void mute()
    {
        AudioListener.volume = 1 - AudioListener.volume;
    }
}
