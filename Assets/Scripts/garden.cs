﻿using UnityEngine;
using System.Collections;


public class garden : MonoBehaviour {
	
	public GameObject flower;
	public GameObject cloud;
	public GameObject ground;
	public int endCoordInt;
	public Sprite sprite1;
	public Sprite sprite2;
	public Sprite sprite3;
	public Sprite sprite4;
	public Sprite sprite5;
	public Sprite sprite6;
	public Sprite cloud1;
	public Sprite cloud2;
	public Sprite cloud3;
	private ArrayList images;
	private ArrayList clouds;
	void Start () {
		images = new ArrayList (){sprite1,sprite2,sprite3,sprite4,sprite5,sprite6};
		clouds = new ArrayList (){cloud1,cloud2,cloud3 };
		SpriteRenderer look = flower.GetComponent<SpriteRenderer>();
		SpriteRenderer cloudLook = cloud.GetComponent<SpriteRenderer>();
		Vector3 pos = new Vector3 (0,-5,5);
		Vector3 cloudPos = new Vector3 (0,0,6);
		Vector3 groundPos = new Vector3 (0,-4.5f,4);
		for(int i=-20;i<=endCoordInt+20;i+=1){
			if (Random.Range (0, 3)==1) {
				look.sprite = (Sprite)images [Random.Range (0, images.Count)];
				pos.x = i;
				Instantiate (flower, pos, Quaternion.identity);
			}
			if (Random.Range (0, 4) == 1) {
				cloudLook.sprite = (Sprite)clouds [Random.Range (0, clouds.Count)];
				cloudPos.x = i;
				cloudPos.y = Random.Range (4, 40);
				Instantiate (cloud, cloudPos, Quaternion.identity);
			}
			if (i % 19 == 0) {
				groundPos.x = i;
				Instantiate (ground, groundPos, Quaternion.identity);
			}
		}
		look.sprite = sprite1;
		cloudLook.sprite = cloud1;
	}

}
