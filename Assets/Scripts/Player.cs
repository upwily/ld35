﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    public AudioClip jumpSoundClip;
    private int deltaX;
    private BoxCollider2D collider;
    private BoxCollider2D jumptrigger;
    private int numOfColliders;
    private List<Collider2D> collidersInCollision;
    public bool hasControl;
    public bool justFlewOff;
    private UI ui;
    // Use this for initialization
    void Start()
    {
        ui = GameObject.Find("Canvas").GetComponent<UI>();
        collider = gameObject.AddComponent<BoxCollider2D>();
        jumptrigger = gameObject.AddComponent<BoxCollider2D>();
        jumptrigger.isTrigger = true;
        jumptrigger.size = new Vector2(6.5f, 4f);
        deltaX = 5;
        GetComponent<Rigidbody2D>().gravityScale = 3;
        AudioSource jumpSound = gameObject.AddComponent<AudioSource>();
        jumpSound.clip = jumpSoundClip;
        hasControl = true;
        justFlewOff = false;
        collidersInCollision = new List<Collider2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "bee")
        {
            ui.stung();
        }
        else if (other.tag == "Finish")
        {
            ui.win();
            collidersInCollision.Add(other);
            numOfColliders++;
        }
        else
        {
            if (other.gameObject.tag == "conveier")
            {
                hasControl = false;
                GetComponent<Rigidbody2D>().gravityScale = 0;

            }
            else if (justFlewOff)
            {
                hasControl = true;
                justFlewOff = false;
            }
            collidersInCollision.Add(other);
            numOfColliders++;
            if (!GetComponent<AudioSource>().isPlaying)
            {
                GetComponent<AudioSource>().Play();
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "bee")
        {
            numOfColliders--;
            collidersInCollision.Remove(other);
            if (!hasControl)
            {
                justFlewOff = true;
                foreach (Collider2D col in collidersInCollision)
                {
                    if (col.gameObject.tag == "conveier")
                    {
                        justFlewOff = false;
                        break;
                    }
                }

                if (justFlewOff)
                {
                    GetComponent<Rigidbody2D>().gravityScale = 3;
                }
            }
            else
            {
                GetComponent<Rigidbody2D>().gravityScale = 3;
            }
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.position.y < -10)
        {
            ui.fallen();
        }
        if (hasControl &&  numOfColliders > 0 && (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 15);
        }
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(gameObject.transform.position, 3);
        if (hitColliders.Length == 0)
        {
            if (numOfColliders != 0)
            {
                Debug.Log("bad");
            }
            numOfColliders = 0;
            collidersInCollision.Clear();
        }

        if (hasControl && Input.GetKeyDown(KeyCode.Escape))
        {
            if (ui.paused)
            {
                ui.continuegame();
            }else
            {
                ui.realpausegame();
            }
        }
        else if (hasControl && (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-deltaX, GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (hasControl && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(deltaX, GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (hasControl)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        }
    }
}
