﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreatesEffector : MonoBehaviour {

    private List<Vector2> vertices;
    private List<Vector2> newPoints;
    private Vector2 normal;
    private Vector2 tangent;
    private PolygonCollider2D pc;
    private PolygonCollider2D realpc;
    private AreaEffector2D ae;
    private List<Rigidbody2D> bodies;

    void OnTriggerStay2D(Collider2D other)
    {
        Vector2 axis = new Vector2(-Mathf.Sin(300f / 180f * Mathf.PI), Mathf.Cos(300f / 180f * Mathf.PI))*20;
        other.gameObject.GetComponent<Rigidbody2D>().AddForce(axis,ForceMode2D.Force);

    }

    // Use this for initialization
    void Start () {
        gameObject.tag = "conveier";
        realpc = GetComponent<PolygonCollider2D>();
        pc = gameObject.AddComponent<PolygonCollider2D>();
        pc.usedByEffector = true;
        pc.isTrigger = true;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        vertices = new List<Vector2>(realpc.points);
        newPoints = new List<Vector2>();
        tangent = vertices[1] - vertices[0];
        if (tangent.y == 0)
        {
            if (tangent.x > 0)
                normal = new Vector2(0, 1);
            else if (tangent.x < 0)
                normal = new Vector2(0, -1);
            else
                normal = Vector2.zero;
        }
        else {
            float t = -tangent.x / tangent.y;
            normal.y = t;
            normal.x = 1;
            normal.Normalize();
            normal = tangent.x > 0 ? -normal : normal;
        }
        newPoints.Add(vertices[0] + normal);
        newPoints.Add(vertices[1] + normal);
        newPoints.Add(vertices[1]);
        newPoints.Add(vertices[0]);
        pc.points = newPoints.ToArray();
    }
}
