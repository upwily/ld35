﻿using UnityEngine;
using System.Collections;

public class triangle : MonoBehaviour {

	// Use this for initialization
	private Vector3 tl = new Vector3(0, 1);
	private Vector3 tr = new Vector3(0, 1);
	private Vector3 bl = new Vector3(-1, 0);
	private Vector3 br = new Vector3(1, 0);
	private Vector3[] newVertices;
	private int[] newTriangles = {0, 1, 3, 1, 2, 3};
	public Mesh mesh;
	void Start() {
		newVertices = new Vector3[] { bl, tl, tr, br };
		mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;

	}
	
	// Update is called once per frame
	void Update () {
		tl.x = (Mathf.Sin (Time.time)-1)/2;
		tr.x = (Mathf.Sin (-Time.time)+1)/2;
		bl.x = -1 - tl.x;
		br.x = 1 - tr.x;
		newVertices = new Vector3[] { bl, tl, tr, br };
		mesh.vertices = newVertices;
		GetComponent<MeshFilter>().mesh = mesh;
	}
}
