﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class sortOfCircle : MonoBehaviour {

	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};

	void Start() {
		List<Vector3> vertices = new List<Vector3>(); 
		vertices.Add( new Vector3 (0, 0));
		for (float i = 0; i <1; i+=(1f/6f)) {
			vertices.Add (new Vector3(i, Mathf.Sqrt (1 - Mathf.Pow (i, 2))));
		}
		for (float i = 1; i >-1; i-=(1f/6f)) {
			vertices.Add (new Vector3(i, -Mathf.Sqrt (1 - Mathf.Pow (i, 2))));
		}
		for (float i = -1; i <=0; i+=(1f/6f)) {
			vertices.Add (new Vector3(i, Mathf.Sqrt (1 - Mathf.Pow (i, 2))));
		}
		newVertices = vertices.ToArray ();
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = spade.MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}
