﻿using UnityEngine;
using System.Collections;

public class rotating : MonoBehaviour {
    public float offset;
	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<Transform>().rotation = new Quaternion(0, 0, Mathf.Cos(Time.time * Mathf.PI / 8 + Mathf.PI * offset), Mathf.Sin(Time.time * Mathf.PI / 8 + Mathf.PI * offset));
        //GetComponent<Transform>().RotateAround(Vector3.forward, -Mathf.PI/4*Time.deltaTime);
    }
}
