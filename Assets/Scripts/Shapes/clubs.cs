﻿using UnityEngine;
using System.Collections;

public class clubs : MonoBehaviour {

	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};

	void Start() {
		newVertices = new Vector3[] {
			new Vector3 (0, 0),
			new Vector3 (0, 1.5f),
			new Vector3 (0.5f, 1.2f),
			new Vector3 (0.6f, 0.6f),
			new Vector3 (0.1f, 0.1f),
			new Vector3 (0.7f,0.4f),
			new Vector3 (1.2f,0.1f),
			new Vector3 (1.2f,-0.4f),
			new Vector3 (0.8f,-0.8f),
			new Vector3 (0.4f,-0.8f),
			new Vector3 (0.1f, -0.2f),
			new Vector3 (0.5f,-2),
			new Vector3 (-0.5f,-2),
			new Vector3 (-0.1f, -0.2f),
			new Vector3 (-0.4f,-0.8f),
			new Vector3 (-0.8f,-0.8f),
			new Vector3 (-1.2f,-0.4f),
			new Vector3 (-1.2f,0.1f),
			new Vector3 (-0.7f,0.4f),
			new Vector3 (-0.1f,0.1f),
			new Vector3 (-0.6f, 0.6f),
			new Vector3 (-0.5f, 1.2f),
		};

		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = spade.MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}
