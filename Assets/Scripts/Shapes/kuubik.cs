﻿using UnityEngine;
using System.Collections;

public class kuubik : MonoBehaviour {

	public Rigidbody2D rigbd;

	// Use this for initialization
	void Start () {
		rigbd = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		rigbd.AddForce (-transform.position);
	}
}
