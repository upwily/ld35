﻿using UnityEngine;
using System.Collections;

public class theBestTriangle : MonoBehaviour {

	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};

	void Start() {
		newVertices = new Vector3[] {
			new Vector3 (0, 0),
			new Vector3 (0, 1),
			new Vector3 (1, -1),
			new Vector3 (-1, -1)
		};

		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = spade.MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}
