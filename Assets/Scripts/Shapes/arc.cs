﻿using UnityEngine;
using System.Collections;

public class arc : MonoBehaviour {

	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};

	void Start() {
		newVertices = new Vector3[] {
			new Vector3 (0, 0),
			new Vector3 (0, 0.5f),
			new Vector3 (1, 0.5f),
			new Vector3 (2, -0.5f),
			new Vector3 (2.5f,-2),
			new Vector3 (1.5f,-2),
			new Vector3 (0.75f,-1f),
			new Vector3 (0,-0.5f),
			new Vector3 (-0.75f,-1f),
			new Vector3 (-1.5f,-2),
			new Vector3 (-2.5f,-2),
			new Vector3 (-2, -0.5f),
			new Vector3 (-1, 0.5f)
		};

		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = spade.MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}
