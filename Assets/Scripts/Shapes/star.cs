﻿using UnityEngine;
using System.Collections;

public class star : MonoBehaviour {
	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};

	void Start() {
		newVertices= new Vector3[] {new Vector3(0,2),new Vector3(0, 4.2f), new Vector3(0.8f, 1.8f),new Vector3(3, 2.5f),new Vector3(1.2f, 0.7f), 
			new Vector3(2, -2), new Vector3(0,0), new Vector3(-2, -2),new Vector3(-1.2f, 0.7f), new Vector3(-3, 2.2f), 
			new Vector3(-0.8f, 1.8f)};
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = spade.MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}