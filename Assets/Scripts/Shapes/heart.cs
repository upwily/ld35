﻿using UnityEngine;
using System.Collections;

public class heart : MonoBehaviour {
	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};

	void Start() {
		newVertices = new Vector3[] { 
			new Vector3 (0, 1.1f), new Vector3 (0, 2.2f), new Vector3 (0.25f, 2.75f), 
			new Vector3 (1, 3), new Vector3 (1.75f, 2.75f), new Vector3 (2, 2),new Vector3 (0, 0),  new Vector3 (-2, 2), 
			new Vector3 (-1.75f, 2.75f), new Vector3 (-1, 3), 
			new Vector3 (-0.25f, 2.75f)
		};
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = spade.MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}
