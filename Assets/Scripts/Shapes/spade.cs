﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class spade : MonoBehaviour {
	public static int[] MakeTriangles(int length){
		List<int> triangles = new List<int> ();
		for (int i = 1; i < length - 1; i++) {
			triangles.Add (0);
			triangles.Add (i);
			triangles.Add (i + 1);
		}
		triangles.Add (0);
		triangles.Add (length-1);
		triangles.Add (1);
		return triangles.ToArray ();
	}
	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};
	void Start() {
		newVertices= new Vector3[] { new Vector3(0,-1), new Vector3(0,0), new Vector3(2, -2),new Vector3(1.75f, -2.75f), 
			new Vector3(1, -3), new Vector3(0.25f, -2.75f), 
			new Vector3(0.1f, -2.2f),new Vector3(0.5f, -4),new Vector3(-0.5f, -4),new Vector3(-0.1f, -2.2f),
			new Vector3(-0.25f, -2.75f), new Vector3(-1, -3), new Vector3(-1.75f, -2.75f), new Vector3(-2, -2)};
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}
