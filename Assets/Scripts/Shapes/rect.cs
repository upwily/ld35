﻿using UnityEngine;
using System.Collections;

public class rect : MonoBehaviour {

	private Vector3[] newVertices;
	private int[] newTriangles;
	private Vector2[] newUV = {};

	void Start() {
		newVertices = new Vector3[] {
			new Vector3 (0,0),
			new Vector3 (0,0.5f),
			new Vector3 (1.5f,2),
			new Vector3 (1.5f,1),
			new Vector3 (-0.5f,-1),
			new Vector3 (-0.5f,0)
		};

		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		newTriangles = spade.MakeTriangles(newVertices.Length);
		mesh.vertices = newVertices;
		mesh.triangles = newTriangles;
		mesh.uv = newUV;
	}
}
