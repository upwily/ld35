﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public GameObject player;
    private float height = 0;
	void Start () {
	}

	// Update is called once per frame
	void LateUpdate () {
        if (player.transform.position.y - height > 3)
        {
            height = player.transform.position.y - 3;
        }
        else if (height - player.transform.position.y > 3)
        {
            height = player.transform.position.y + 3;
        }
        transform.position = new Vector3(player.transform.position.x + 5, height, -10);
	}
}
