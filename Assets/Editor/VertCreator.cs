﻿using UnityEditor;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class VertCreator {

    private static int[] MakeTriangles(int length)
    {
        List<int> triangles = new List<int>();
        for (int i = 1; i < length - 1; i++)
        {
            triangles.Add(0);
            triangles.Add(i);
            triangles.Add(i + 1);
        }
        triangles.Add(0);
        triangles.Add(length - 1);
        triangles.Add(1);
        return triangles.ToArray();
    }

    [MenuItem("Assets/Generate Mesh")]
    public static void GenerateMesh(MenuCommand menuCommand)
    {
        String name = Selection.activeObject.name;
        string path = EditorUtility.SaveFilePanel("Save Mesh Asset", "Assets/Meshes", name+".asset", "asset");
        if (string.IsNullOrEmpty(path)) return;

        String relativepath = FileUtil.GetProjectRelativePath(path);

        List<Vector3> vertices = new List<Vector3>();

        var reader = new StreamReader(File.OpenRead("Assets/Vertices/"+name+".csv"));
        List<string> listA = new List<string>();
        List<string> listB = new List<string>();
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var values = line.Split(';');

            vertices.Add(new Vector3(float.Parse(values[0]), float.Parse(values[1])));
        }

        Mesh mesh = new Mesh();
        Vector2[] uv = { };
        int[] triangles = MakeTriangles(vertices.Count);


        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.Optimize();
        mesh.RecalculateBounds();
        AssetDatabase.CreateAsset(mesh, relativepath);
        AssetDatabase.SaveAssets();
    }
}
